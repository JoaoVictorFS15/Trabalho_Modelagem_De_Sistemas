package modelo;


import java.util.ArrayList;

import java.util.List;





public class Compra {
	private int numero;
	private String dataDaCompra;
	
	private List<ItemCompra> itens = new ArrayList<ItemCompra>();
	
	public Compra(int numero) {
		this.numero = numero;
	}
	
	public void addItemCompra(ItemCompra item){
		itens.add(item);
	}
	
	public void removeItemCompraPorDescricao(String descricao){
		for (ItemCompra item: itens){
			if (item.getDescricao().equals(descricao)){
				itens.remove(item);
				break;
			}
		}
	}	

	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getDataDaCompra() {
		return dataDaCompra;
	}
	public void setDataDaCompra(String dataDaCompra) {
		this.dataDaCompra = dataDaCompra;
	}
	
	public float totalizarItens(){
		float total=0;
		for (ItemCompra item:itens){
			total = total + item.getQuantidade()*item.getValor();
		}
		return total;
	}
	
	public List<ItemCompra> getItens(){
		return itens;
	}
	
	

}
