package modelo;

public class Produto {
	private int codigo;
	private String descricao;
	private float valor;
	private int quantidadeEmEstoque;
	private String dataDeCadastro;
	
	public Produto() {}
	
	public Produto(int codigo,String descricao, float valor,int quantidadeEmEstoque,String dataDeCadastro) {
		this.codigo=codigo;
		this.descricao = descricao;
		this.valor = valor;
		this.quantidadeEmEstoque = quantidadeEmEstoque;
		this.dataDeCadastro= dataDeCadastro;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}
	public int getQuantidadeEmEstoque() {
		return quantidadeEmEstoque;
	}
	public void setQuantidadeEmEstoque(int quantidadeEmEstoque) {
		this.quantidadeEmEstoque = quantidadeEmEstoque;
	}
	public String getDataDeCadastro() {
		return dataDeCadastro;
	}
	public void setDataDeCadastro(String dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}

	public void baixarProduto(int quantBaixada){
		if (quantBaixada <= this.quantidadeEmEstoque){
			this.quantidadeEmEstoque = this.quantidadeEmEstoque - quantBaixada;
		}	
	}
	

}
